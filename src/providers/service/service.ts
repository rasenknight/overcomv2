import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiceProvider {

  constructor(private http: Http) {
    console.log('Hello ServiceProvider Provider');
  }
public getListUser(intro: string){
        return new Promise((resolve, reject)=>{
        this.http.get('http://localhost/hero_call.php?hero_name='+intro).map(res=>res.json())
        .subscribe((data:any = []) =>{
            resolve(data);
        });
    })
}
public getListMaps(intro: string){
    return new Promise((resolve, reject)=>{
        this.http.get('http://localhost/map_call.php?map_class='+intro).map(res=>res.json())
        .subscribe((data:any = []) =>{
            resolve(data);
        });
    })
}
public getProfile(id: string){
        return new Promise((resolve, reject)=>{
        this.http.get('http://localhost/profile_call.php?ID='+id).map(res=>res.json())
        .subscribe((data:any = []) =>{
            resolve(data);
        });
    })
} 
public getMapProfile(id: string){
        return new Promise((resolve, reject)=>{
        this.http.get('http://localhost/mapprofile_call.php?ID='+id).map(res=>res.json())
        .subscribe((data:any = []) =>{
            resolve(data);
        });
    })
}      
    
}
