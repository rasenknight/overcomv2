import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { ServiceProvider } from '../../providers/service/service';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
    public list: string[];
    public obj;
    searchQuery: string = '';
    
    public initializeProfile(id: string){
        this.obj.getProfile(id).then(results =>{
          console.log(results);
          this.list = results;
      }).catch(err =>{
          console.log(err);
      });
    }

  constructor(public navCtrl: NavController, public navParams: NavParams, public profile:ServiceProvider) {
      console.log(navParams.get('val'));
      this.obj=profile;
      this.initializeProfile(navParams.get('val'));
  }
    

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
