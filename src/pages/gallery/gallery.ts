import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { ServiceProvider } from '../../providers/service/service';
import { ProfilePage } from '../profile/profile';

/**
 * Generated class for the GalleryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})
export class GalleryPage {
    public obj;
    searchQuery: string = '';
public list: string[];
public initializeItems(intro: string){
        this.obj.getListUser(intro).then(results =>{
          console.log(results);
          this.list = results;
      }).catch(err =>{
          console.log(err);
      });
    }
  constructor(public navCtrl: NavController, public navParams: NavParams,public users:ServiceProvider) {
      console.log(navParams.get('val'));
      this.obj=users;
      this.initializeItems('');
  }
  toprofile(id: string){
        this.navCtrl.push(ProfilePage,{
            val: id
        })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad GalleryPage');
  }
   getItems(ev: any) {
    // Reset items back to all of the items
    // set val to the value of the searchbar
    let val = ev.target.value;
       this.initializeItems(val.toString());

    // if the value is an empty string don't filter the items
  }
}
