import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GalleryPage } from '../gallery/gallery';
import { MapPage } from '../map/map';
 @Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
    load(){
        this.navCtrl.push(GalleryPage,{
            val: 'gotogallery'
        })
    }
    load2(){
        this.navCtrl.push(MapPage,{
            val: 'gotomap'
        })
    }
}
