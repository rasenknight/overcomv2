import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { ServiceProvider } from '../../providers/service/service';
import { MapProfilePage } from '../map-profile/map-profile';

/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
    public obj;
    searchQuery: string = '';
public list: string[];
public initializeMaps(intro: string){
        this.obj.getListMaps(intro).then(results =>{
          console.log(results);
          this.list = results;
      }).catch(err =>{
          console.log(err);
      });
    }
  constructor(public navCtrl: NavController, public navParams: NavParams,public maps:ServiceProvider) {
      console.log(navParams.get('val'));
      this.obj=maps;
      this.initializeMaps('all');
  }
    tomapprofile(id: string){
        this.navCtrl.push(MapProfilePage,{
            val: id
        })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
  }
    radioChecked(par:string){
        this.initializeMaps(par);
    }

}
