import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { GalleryPage } from '../pages/gallery/gallery';
import { MapPage } from '../pages/map/map';
import { MapProfilePage } from '../pages/map-profile/map-profile';
import { ProfilePage } from '../pages/profile/profile';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ServiceProvider } from '../providers/service/service';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    GalleryPage,
      ProfilePage,
      MapPage,
      MapProfilePage
  ],
  imports: [
    BrowserModule,
      HttpModule,
      CommonModule,
    IonicModule.forRoot(MyApp),
      IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    GalleryPage,
      ProfilePage,
      MapPage,
      MapProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ServiceProvider]
})
export class AppModule {}
