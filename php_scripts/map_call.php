<?php  
// Declaramos el header como aplicación json y que interprete los caracteres especial UTF-8
  header("Access-Control-Allow-Origin: *");
  header("Content-Type: application/json; charset=UTF-8");
 
  // Conectamos a la base de datos y hacemos un select
  $conn = new mysqli("localhost", "root", "", "ow_database");
 
  $result = $conn->query("SELECT * FROM ow_map");
 
  $outp = "";
  
  // Formateamos nuestro JSON
  while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
      if ($outp != "") {$outp .= ",";}
      $outp .= '{"map_name":"'  . $rs["map_name"] . '",';
      $outp .= '"map_img":"'   . $rs["map_img"]        . '",';
      $outp .= '"map_class_icon":"'   . $rs["map_class_icon"]        . '"}';
  }
  $outp ='['.$outp.']';
  $conn->close();
 
  echo($outp);
?>