<?php  
// Declaramos el header como aplicación json y que interprete los caracteres especial UTF-8
  header("Access-Control-Allow-Origin: *");
  header("Content-Type: application/json; charset=UTF-8");
 
  // Conectamos a la base de datos y hacemos un select
  $conn = new mysqli("localhost", "root", "", "ow_database");
  $ID=$_GET['ID'];
  $result = $conn->query("SELECT * FROM ow_hero where ID = ".$ID);

  $outp = '';
  
  // Formateamos nuestro JSON
  while($rs = $result->fetch_array(MYSQLI_ASSOC)){
      if ($outp != "") {$outp .= ",";}
      $outp .= '{"hero_name":"'  . $rs["hero_name"] . '",';
      $outp .= '"hero_img":"'   . $rs["hero_img"]        . '",';
      $outp .= '"hero_class":"'   . $rs["hero_class"]        . '",';
      $outp .= '"hero_home":"'   . $rs["hero_home"]        . '",';
      $outp .= '"hero_img_full":"'   . $rs["hero_img_full"]        . '",';
      $outp .= '"hero_fullname":"'   . $rs["hero_fullname"]        . '",';
      $outp .= '"hero_description":"'   . $rs["hero_description"]        . '",';
      $outp .= '"hero_age":"'   . $rs["hero_age"]        . '",';
      $outp .= '"hero_class_icon":"'   . $rs["hero_class_icon"]        . '"}';
  }
  $outp ='['.$outp.']';
  $conn->close();
 
  echo ($outp);
?>